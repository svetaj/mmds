import java.awt.Point;

public class CURE {
    public static void main(String[] args) {
        Point x = new Point(0,0);
        Point y = new Point(10,10);
        Point a = new Point(1,6);
        Point b = new Point(3,7);
        Point c = new Point(4,3);
        Point d = new Point(7,7);
        Point e = new Point(8,2);
        Point f = new Point(9,5);

        Point[] p = {x,y,a,b,c,d,e,f};
        int[] ind =   {1,1,0,0,0,0,0,0};   // x, y manually selected in advance

        String[] cind =   {"x","y","a","b","c","d","e","f"};

        double mindist, maxdist, dst;
        int k,m = 0;
        for (int ii = 0; ii < p.length; ii++) {
            maxdist = 0.0; m = -1;
            for (int i = 0; i < p.length; i++) {
                if (ind[i] != 0) continue;
                mindist = Double.MAX_VALUE;
                for (int j = 0; j < p.length; j++) {
                     if (ind[j] == 0) continue;
                     dst = p[i].distance(p[j]);
                     if (dst < mindist) mindist = dst;
                }
                if (mindist > maxdist) { maxdist = mindist; m = i; }
            }
            if (m > 0) System.out.println("Added "+cind[m]);
            if (m > 0) ind[m] = 1;
        }

    }
}