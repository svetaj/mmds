import java.awt.Point;

public class Centroid1x {
    public static void main(String[] args) {
        Point[] p = {

// Yellow: UL=(7,12) and LR=(12,8); Blue: UL=(16,16) and LR=(18,5)
            new Point(7,12),   // A1 Y UL
            new Point(12,8),   // A1 Y LR
            new Point(7,8),    // A1 Y LL
            new Point(12,12),   // A1 Y UR

            new Point(16,16),  // A1 B UL
            new Point(18,5),  // A1 B LR
            new Point(16,5),  // A1 B LL
            new Point(18,16),  // A1 B LR

// Yellow: UL=(6,7) and LR=(11,4); Blue: UL=(11,5) and LR=(17,2)
            new Point(6,7),    // A2 Y UL
            new Point(11,4),   // A2 Y LR
            new Point(6,4),    // A2 Y UL
            new Point(11,7),   // A2 Y LR

            new Point(11,5),  // A2 B UL
            new Point(17,2),   // A2 B LR
            new Point(11,2),   // A2 B UL
            new Point(17,5),  // A2 B LR

// Yellow: UL=(7,12) and LR=(12,8); Blue: UL=(16,19) and LR=(25,12)
            new Point(7,12),    // A3 Y UL
            new Point(12,8),    // A3 Y LR
            new Point(7,8),     // A3 Y UL
            new Point(12,12),   // A3 Y LR

            new Point(16,19),    // A3 B UL
            new Point(25,12),    // A3 B LR
            new Point(16,12),    // A3 B UL
            new Point(25,19),    // A3 B LR

// Yellow: UL=(6,15) and LR=(13,7); Blue: UL=(16,19) and LR=(25,12)
            new Point(6,15),     // A3 Y UL
            new Point(13,7),    // A3 Y LR
            new Point(6,7),     // A3 Y UL
            new Point(13,15),    // A3 Y LR

            new Point(16,19),   // A3 B UL
            new Point(25,12),    // A3 B LR
            new Point(16,12),    // A3 B UL
            new Point(25,19),   // A3 B LR

         };

        Point[] c = {
            new Point(5,10),
            new Point(20,5),
        };

        int[] xx = {1,1,1,1,2,2,2,2,
                    3,3,3,3,4,4,4,4,
                    5,5,5,5,6,6,6,6,
                    7,7,7,7,8,8,8,8,
                    9,9,9,9,10,10,10,10
                   };
        int[] y = new int[32];

        double dist, d;
        int k;
        for (int i = 0; i < p.length; i++) {
            dist = Double.MAX_VALUE;
            k = 0;
            for (int j = 0; j < c.length; j++) {
                 d = p[i].distance(c[j]);
                 if (d < dist) {dist = d; k = j;}
            }
            y[i] = k;
        }

        for (int i = 0; i < p.length; i++) {
            System.out.printf("box %d %d centroid (%f, %f)\n", xx[i], y[i], p[i].getX(), p[i].getY());
        }
    }
}