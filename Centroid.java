import java.awt.Point;

public class Centroid {
    public static void main(String[] args) {
        Point[] p = {
        new Point(28,145),    // 0
        new Point(25,125),    // 1
        new Point(50,130),    // 0
        new Point(65,140),    // 0
        new Point(38,115),    // 0
        new Point(55,118),    // 0
        new Point(44,105),    // 1
        new Point(29,97),     // 1
        new Point(50,90),     // 0
        new Point(63,88),     // 0
        new Point(43,83),     // 0
        new Point(35,63),     // 1
        new Point(55,63),     // 1
        new Point(42,57),     // 1
        new Point(50,60),     // 0
        new Point(23,40),     // 1
        new Point(64,37),     // 1
        new Point(50,30),     // 0
        new Point(33,22),     // 1
        new Point(55,20)      // 1
         };
        int[] x = {0,1,0,0,0,0,2,3,0,0,
                   0,4,5,6,0,7,8,0,9,10 };
        int[] y = {0,1,0,0,0,0,2,3,0,0,
                   0,4,5,6,0,7,8,0,9,10 };


        for (int i = 0; i < p.length; i++) {
            System.out.printf("%d centroid (%f, %f)\n", x[i], p[i].getX(), p[i].getY());
        }

        double dist, d;
        int k;
        for (int i = 0; i < p.length; i++) {
            if (x[i] > 0) continue;
            dist = Double.MAX_VALUE;
            k = 0;
            for (int j = 0; j < p.length; j++) {
                 if (x[j] == 0) continue;
                 d = p[i].distance(p[j]);
                 if (d < dist) {dist = d; k = x[j];}
            }
            y[i] = k;
        }

        for (int i = 0; i < p.length; i++) {
            System.out.printf("%d centroid (%f, %f)\n", y[i], p[i].getX(), p[i].getY());
        }

        double count;
        double sumx, sumy;
        for (int j = 0; j < 10; j++) {
            count = 0.0; sumx = 0.0; sumy = 0.0;
            for (int i = 0; i < p.length; i++) {
                if (y[i] != j+1) continue;
                count++;
                sumx = sumx + p[i].getX();
                sumy = sumy + p[i].getY();
            }
            System.out.printf("cluster %d centroid (%f, %f)\n", j+1, sumx/count, sumy/count);
        }
    }
}