import java.awt.Point;

public class Centroid1 {
    public static void main(String[] args) {
        Point[] p = {
            new Point(3,3),    // A1 Y UL
            new Point(10,1),   // A1 Y LR
            new Point(3,1),    // A1 Y LL
            new Point(10,3),   // A1 Y UR
            new Point(15,14),  // A1 B UL
            new Point(20,10),  // A1 B LR
            new Point(15,10),  // A1 B LL
            new Point(20,14),  // A1 B LR

            new Point(3,3),    // A2 Y UL
            new Point(10,1),   // A2 Y LR
            new Point(3,1),    // A2 Y UL
            new Point(10,3),   // A2 Y LR
            new Point(13,10),  // A2 B UL
            new Point(16,4),   // A2 B LR
            new Point(13,3),   // A2 B UL
            new Point(16,10),  // A2 B LR

            new Point(3,15),    // A3 Y UL
            new Point(13,7),    // A3 Y LR
            new Point(3,7),     // A3 Y UL
            new Point(13,15),   // A3 Y LR
            new Point(11,5),    // A3 B UL
            new Point(17,2),    // A3 B LR
            new Point(11,2),    // A3 B UL
            new Point(17,5),    // A3 B LR

            new Point(7,8),     // A3 Y UL
            new Point(12,5),    // A3 Y LR
            new Point(7,5),     // A3 Y UL
            new Point(12,8),    // A3 Y LR
            new Point(13,10),   // A3 B UL
            new Point(16,4),    // A3 B LR
            new Point(13,4),    // A3 B UL
            new Point(16,10),   // A3 B LR

         };

        Point[] c = {
            new Point(5,10),
            new Point(20,5),
        };

        int[] xx = {1,1,1,1,2,2,2,2,
                    3,3,3,3,4,4,4,4,
                    5,5,5,5,6,6,6,6,
                    7,7,7,7,8,8,8,8,
                    9,9,9,9,10,10,10,10
                   };
        int[] y = new int[32];

        double dist, d;
        int k;
        for (int i = 0; i < p.length; i++) {
            dist = Double.MAX_VALUE;
            k = 0;
            for (int j = 0; j < c.length; j++) {
                 d = p[i].distance(c[j]);
                 if (d < dist) {dist = d; k = j;}
            }
            y[i] = k;
        }

        for (int i = 0; i < p.length; i++) {
            System.out.printf("box %d %d centroid (%f, %f)\n", xx[i], y[i], p[i].getX(), p[i].getY());
        }
    }
}