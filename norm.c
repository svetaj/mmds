#include <stdio.h>
#include <math.h>

double norm1(double x1, double y1, double x2, double y2)
{
	return (fabs(x1-x2)+fabs(y1-y2));
}

double norm2(double x1, double y1, double x2, double y2)
{
	return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}

main()
{
	int m = 2;
	double xx[] = {0.0, 100.0};
	double yy[] = {0.0, 40.0};

	int n = 4;
//	double x[] = {53.0, 53.0, 63.0, 56.0};
//	double y[] = {18.0, 10.0, 8.0, 13.0};
	double x[] = {56.0, 58.0, 52.0, 57.0};
	double y[] = {15.0, 13.0, 13.0, 5.0};

    int i, j;

    for (i = 0; i<n; i++) {
		for (j = 0; j<m; j++) {
			printf("L1 NORM (%f,%f) (%f,%f) = %f\n", x[i], y[i], xx[j], yy[j], norm1(x[i], y[i], xx[j], yy[j]));
			printf("L2 NORM (%f,%f) (%f,%f) = %f\n", x[i], y[i], xx[j], yy[j], norm2(x[i], y[i], xx[j], yy[j]));
			printf("\n");
		}
	}
}