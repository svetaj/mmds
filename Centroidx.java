import java.awt.Point;

public class Centroidx {
    public static void main(String[] args) {
        Point[] p = {
        new Point(28,145),    // 0
        new Point(25,125),    // 1
        new Point(50,130),    // 0
        new Point(65,140),    // 0
        new Point(38,115),    // 0
        new Point(55,118),    // 0
        new Point(44,105),    // 1
        new Point(29,97),     // 1
        new Point(50,90),     // 0
        new Point(63,88),     // 0
        new Point(43,83),     // 0
        new Point(35,63),     // 1
        new Point(55,63),     // 1
        new Point(42,57),     // 1
        new Point(50,60),     // 0
        new Point(23,40),     // 1
        new Point(64,37),     // 1
        new Point(50,30),     // 0
        new Point(33,22),     // 1
        new Point(55,20)      // 1
         };
        int[] y = {0,1,0,0,0,0,2,3,0,0,
                   0,4,5,6,0,7,8,0,9,10 };

        Point[] cent = new Point[10];

        System.out.printf("Before computation \n");
        for (int i = 0; i < p.length; i++) {
            if (y[i] <= 0) continue;
            cent[y[i]-1] = p[i];
            System.out.printf("%d centroid (%f, %f)\n", y[i], p[i].getX(), p[i].getY());
        }

        double dist, d;
        int k;
        int noclust = 10;

        for (int i = 0; i < p.length; i++) {
            dist = Double.MAX_VALUE;
            k = 0;
            for (int j = 0; j < noclust; j++) {
                 d = p[i].distance(cent[j]);
                 if (d < dist) {dist = d; k = j+1;}
            }
            y[i] = k;
        }

        System.out.printf("First computation with existing centroids\n");
        for (int i = 0; i < p.length; i++) {
            System.out.printf("%d centroid (%f, %f)\n", y[i], p[i].getX(), p[i].getY());
        }

        System.out.printf("New centroids computation\n");
        double count;
        double sumx, sumy;
        for (int j = 0; j < noclust; j++) {
            count = 0.0; sumx = 0.0; sumy = 0.0;
            for (int i = 0; i < p.length; i++) {
                if (y[i] != j+1) continue;
                count++;
                sumx = sumx + p[i].getX();
                sumy = sumy + p[i].getY();
            }
            System.out.printf("cluster %d centroid (%f, %f)\n", j+1, sumx/count, sumy/count);
            cent[j] = new Point((int)(sumx/count), (int)(sumy/count));
        }

        System.out.printf("List again new centroids\n");
        for (int i = 0; i < noclust; i++) {
            System.out.printf("%d centroid IS (%f, %f)\n", i+1, cent[i].getX(), cent[i].getY());
        }

        System.out.printf("Computation with new centroids\n");
        for (int i = 0; i < p.length; i++) {
            dist = Double.MAX_VALUE;
            k = 0;
            for (int j = 0; j < noclust; j++) {
                 d = p[i].distance(cent[j]);
                 if (d < dist) {dist = d; k = j+1;}
            }
            y[i] = k;
        }

        for (int i = 0; i < p.length; i++) {
            System.out.printf("%d centroid (%f, %f)\n", y[i], p[i].getX(), p[i].getY());
        }

    }
}